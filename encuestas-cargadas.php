<?php
  $id = $_POST['fname'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
    <title>Formularios</title>

    <!-- html2pdf -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> -->
    <!-- jsPDF -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script> -->
    <!-- Estilos -->
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/styles2.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Didact+Gothic&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
</head>

<body>
    <!-- Header -->
  <div class="header-1">
    <div class="left-nav">
       <div class="nav-items">
        <div class="xlotl-creative-labs">XÓLOTL CREATIVE LABS</div>
      </div>
     </div>
  </div>
    <div class="container">
        <!-- Area de las graficas -->
        <main>
            <div class="botonDescarga">
                <!-- <input id="botonDeDescarga1" type="button" onclick="tableToExcel('tblData', 'W3C Example Table')" value="Descargar Datos a Excel"> -->
                <input id="botonDeDescarga" type="button" value="Descargar Reporte en PDF" onclick="descargar();">
                <input id="botonDeDescarga1" type="button" value="Descargar Datos en Excel" onclick="Exportar()">
            </div>
            <div class="main__container" id="reportPage">
                <div class="encuestas-tabla-container">
                    <div class="encuesta-por-nombre" >
                        <h2 id="titulo__encuesta"> </h2>
                        <p id="descripcion__encuesta"></p>
                    </div>
                    <br>
                    <div class="main-tabla">
                        <table class="segunda-tabla table-fixed" id="segunda-tabla">
                            <tr>
                                <th>No.</th>
                                <th>Nombre</th>
                            </tr>
                        </table>
                    </div>
                    <div id="graficas-pregunta">
                        
                    </div>
                    <br>
                    <div class="card2" id="card2">
                        
                    </div>
                </div>
            </div>
            <div class="main-tabla">
                <table class="segunda-tabla" id="tblData" style="display: none; "><!--  -->
                </table>
            </div>
            <div class="main-tabla" style="display: none;">
                <table id="primera__tabla" class="primera-tabla">
                    <tr>
                        <th>Realizadas: 0</th>
                    </tr>
                </table>
                <table id="primera__tabla2" class="primera-tabla">
                    <tr>
                        <th>Cargadas</th>
                    </tr>
                </table>
            </div>
        </main>

       <!-- Menu -->
    <div id="sidebar">
      <div class="info_usuario">
        <div class="imagen_usuario">
          <img src="imagenes/user.png" alt="" class="usuario">
          <div class="close-sidebar" onclick="closeSidebar()">
            <img src="imagenes/close.png" alt="" class="close">
          </div>
        </div>

        <div id='nombre_usuario' class="nombre_usuario">

        </div>
        <div id="encuestas">
          <br>
          <nav class="encuestas">
            <ul>
              <a href="dashboard.html">
                <span class="titulo-formularios">Formularios</span>
              </a>
              <div class="imagen-formularios">
                <img
                  src="imagenes/formulario.svg"
                  class="imagen-formularios-tamaño"
                />
              </div>
              <li class="submenu">
                <a href="usuarios.html">Usuarios</a>
              </li>
              <li class="submenu">
                <a href="preguntas.html">Preguntas</a>
              </li>
              <li class="submenu">
                <a href="respuestas.html">Respuestas</a>
              </li>
              <li class="submenu">
                <a id="encuestas__realizadas" title="Encuestas contestadas en la Aplicación" href="#"><img src="imagenes/flecha-hacia-abajo-para-navegar.png"
                    class="caret">Encuestas Realizadas: 0 </a>
                <ul id="numero__realizadas" class="children">

                </ul>
              </li>
              <li class="submenu">
                <a href="#" title="Encuestas Creadas en la Aplicación"><img src="imagenes/flecha-hacia-abajo-para-navegar.png" class="caret">Encuestas
                  Cargadas</a>
                <ul id="encuestas__cargadas" class="children">

                </ul>
              </li>
              <li class="submenu">
                <a href="grafica-tipo.html">Tipos de pregunta</a>
              </li>
              <li class="submenu">
                <a href="grafica-preguntas.html">Preguntas por formulario</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="informacion-contacto">
          <div class="title-informacion">
            <h4>Información de contacto</h4>
          </div>
          <br>
          <div>
            <a href="#"><img src="imagenes/correo-electronico.png" alt="">contacto@xolotlcl.com</a>
          </div>
          <div>
            <a href="#"><img src="imagenes/llamada-telefonica.png" alt="">771 410 6182</a>
          </div>
        </div>
        <div class="datos-usuario">
          <span class="nombre-usuario"></span>
          <span class="correo-usuario"></span>
          <img
            src="imagenes/usuario.png"
            class="imagen-usuario"
          />
        </div>
        <div class="configuracion">
          <img
            src="imagenes/config.svg"
            class="config-img"
          />
          <span class="config-text">Configuración</span>
        </div>
        <div class="salir">
          <img
            src="imagenes/salir.svg"
            class="config-img"
          />
          <button class="config-text" onclick="cerrar()">Salir</span>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <div class="footer-mini-7">
    <div class="xolotl-creative-labs-1">XÓLOTL CREATIVE LABS</div>
    <img class="divider-1" src="imagenes/divider-3.svg" />
    <div class="flex-row-1">
      <p class="titulo">© 2022 XÓLOTL Creative Labs SA de R.L. de C.V.</p>
      <div class="social-links">
      <a href="https://www.instagram.com/xolotlcl/" target="_blank">
            <img class="social-icons-1" src="imagenes/social-icons.svg" />
          </a>
          <a href="http://www.xolotlcl.com/" target="_blank">
            <img class="social-icons" src="imagenes/social-icons-1.svg"/>
          </a>
          <a href="https://twitter.com/xolotlcl?lang=es" target="_blank">
            <img class="social-icons" src="imagenes/social-icons-2.svg" />
          </a>
          <img class="social-icons" src="imagenes/social-icons-3.svg"/>
      </div>
    </div>
  </div>
        <!-- Scripts -->
        <!-- The core Firebase JS SDK is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/8.2.10/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.2.10/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/8.2.10/firebase-database.js"></script>

        <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
        <script src="https://www.gstatic.com/firebasejs/8.2.10/firebase-analytics.js"></script>

        <script src="scripts/fs-encuestas1.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
        <script src="https://code.jquery.com/jquery-latest.js"></script>
        <script src="scripts/script.js" charset="UTF-8"></script>
        <script type="text/javascript" src="scripts/descargarReporte.js"></script>
        <script>
            var numero = <?php echo $id ?>;
            encuestas(numero);
        </script>
</body>

</html>