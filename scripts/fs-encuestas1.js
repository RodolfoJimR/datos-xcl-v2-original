var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        var uid = user.uid;
        datos__firebase(uid);
    } else {
        // User is signed out
        // ...
    }
});

const database = firebase.database();

// datos que se van a estraer de la base de datos
function datos__firebase(id) {

    // mostrar nombre del usuario
    database.ref(`/Usuarios/${id}/nombre`).orderByValue().on('value', snapshot => {
        var nombre_usuario = snapshot.val()
        var contenido = document.getElementById('nombre_usuario');
        contenido.innerHTML = `<br><div class="datos-usuario"><span class="nombre-usuario">${nombre_usuario}</span></div>`;
    });

    //mostrar las encuestas realizadas
    database.ref((`/Usuarios/${id}/Respuestas`)).on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        var encuesta = ``;
        var i = 0;
        for(array in nombre_usuario2){

            var encuesta = encuesta + `<li><form action="encuestas-realizadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${array}"></p><input type="submit" value="Encuesta ${array}" /></form></li>`;

            i++;
        }
        var numero__encuestas__realizadas = `<img src="imagenes/flecha-hacia-abajo-para-navegar.png" class="caret">Encuestas Realizadas: ${i}`;

        var contenido = document.getElementById('encuestas__realizadas');
        contenido.innerHTML = numero__encuestas__realizadas;
        var contenido = document.getElementById('numero__realizadas');
        contenido.innerHTML = encuesta;
    });

    // para mostrar las encuestas cargadas con el numero de usuarios por encuesta
    database.ref((`/Usuarios/${id}/Formularios`)).orderByValue().on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        nombre_usuario2 = nombre_usuario2.length;
        primera_funcion(id, nombre_usuario2);
    });

    database.ref(`/Usuarios/${id}/Formularios/${window.num}/titulo`).on('value', snapshot => {
        var titulo = snapshot.val();
        var contenido = document.getElementById('titulo__encuesta');
        contenido.innerHTML = titulo;
    });
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/descripcion`).on('value', snapshot => {
        var descripcion = snapshot.val();
        var contenido = document.getElementById('descripcion__encuesta');
        contenido.innerHTML = descripcion;
    }); 
    usuarios(id);
    respuestas(id);
}

// funcion para mostrar las encustas cargadas en el menu 
var arrayUsuarios = [];
function primera_funcion(id, cantidad) {
    var arraycantidadUsuarios = [];
    var j = 0;
    for (var i = 1; i < cantidad; i++) {
        database.ref('/Usuarios/' + id + '/Formularios/' + i + '/id_usuario_origen').on('value', snapshot => {
            var formularios = snapshot.val();
            arrayUsuarios.push(formularios);
        });
    }
    segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
}

function segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j) {
    database.ref('/Usuarios/' + id + '/quien_responde/' + j).on('value', snapshot => {
        usuarios_encuesta = snapshot.val();
        if (usuarios_encuesta != null) {
            var l = 0;
            for (var k = 0; k < 50; k++) {
                if (usuarios_encuesta[k] != undefined) {
                    l++;
                }
            }
        } else {
            l = 0;
        }
        arraycantidadUsuarios.push(l);
        tercera_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
    });
}

function tercera_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j) {
    var encuesta = ``;
    j++;
    if (j < cantidad) {
        segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
    } else {
        if (arraycantidadUsuarios.length != arrayUsuarios.length) {
            arrayUsuarios.unshift(null);
        }
        for (var i = 0; i < cantidad; i++) {
            if (arrayUsuarios[i] == id) {
                encuesta = encuesta + `<li><form action="encuestas-cargadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="` + i + `"></p><input type="submit" value="Encuesta ` + i + ` : ` + arraycantidadUsuarios[i] + ` Usuarios" /></form></li>`;
            }
        }
        var contenido2 = document.getElementById('encuestas__cargadas');
        contenido2.innerHTML = encuesta;
    }
}
//funciones para que muestre el numero de usuarios por encuesta.
function usuarios(id) {
    let cantidadPorUsuario =0;
    var datos__usuario = `<tr><th>No.</th><th>Nombre</th></tr>`;
    database.ref(`/Usuarios/${id}/quien_responde/${window.num}`).orderByValue().on('value', snapshot => {
        var usuariosPorEncuesta = snapshot.val();
        for(array in usuariosPorEncuesta){
            if(array != 'n_quien'){
                database.ref(`/Usuarios/${id}/quien_responde/${window.num}/${array}`).orderByValue().on('value', snapshot => {
                    var idUsuario = snapshot.val();
                    database.ref(`/Usuarios/${idUsuario}/nombre`).orderByValue().on('value', snapshot => {
                        var nombreUsuario = snapshot.val();
                        cantidadPorUsuario++;
                        datos__usuario = datos__usuario + `<tr><td>${cantidadPorUsuario}</td><td>${nombreUsuario}</td></tr>`;
                        var contenido = document.getElementById('segunda-tabla');
                        contenido.innerHTML = datos__usuario;
                    });
                });
            }
        }
        
    });
}

// funciones para extraer las respuestas del usuario
var idUsuariosPorEncuesta = [];
const respuestas = (id) =>{
    database.ref(`/Usuarios/${id}/quien_responde/${window.num}`).orderByValue().on('value', snapshot =>{
        var idUsuarios = snapshot.val();
        for(array in idUsuarios){
            if(array != 'n_quien'){
                idUsuariosPorEncuesta.push(array);
            }
        }
    });
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/titulo`).on('value', snapshot =>{
        var tituloEncuestaOriginal = snapshot.val();
        database.ref(`/Usuarios/${id}/quien_responde/${window.num}`).orderByValue().on('value', snapshot =>{
            var idUsuarios = snapshot.val();
            for(array in idUsuarios){
                if(array != 'n_quien'){
                    respuestas2(id, idUsuarios[array], tituloEncuestaOriginal, idUsuariosPorEncuesta.length);
                }
            }
        });
    });
}

const respuestas2 = (id, id2, tituloEncuestaOriginal, cantidadDeUsuarios) =>{
    database.ref(`/Usuarios/${id2}/Respuestas`).orderByValue().on('value', snapshot =>{
        var respuestasPorUsuario = snapshot.val();
        for(array2 in respuestasPorUsuario){
            database.ref(`/Usuarios/${id2}/Formularios/${array2}/titulo`).on('value', snapshot =>{
                window.titulo = snapshot.val();
            });
            database.ref(`/Usuarios/${id2}/Formularios/${array2}/id_usuario_origen`).on('value', snapshot =>{
                window.idOrigen = snapshot.val();
            });
            database.ref(`/Usuarios/${id2}/Formularios/${array2}/id`).on('value', snapshot =>{
                var idPregunta = snapshot.val();
                if(tituloEncuestaOriginal == window.titulo && window.idOrigen == id){
                    database.ref(`/Usuarios/${id2}/Respuestas/${idPregunta}`).on('value', snapshot =>{
                        var respuestas = snapshot.val();
                        respuestas3(id, respuestas, cantidadDeUsuarios);
                    });
                }
            });
        }
    });
}

let tipoDePregunta = [];
let nombreDePregunta = [];
let todasLasRespuestas = [];
var i = 0;
const respuestas3 = (id, respuestas, cantidadDeUsuarios) =>{
    todasLasRespuestas.push(respuestas);
    i++;
    if(cantidadDeUsuarios == 1){
        database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas`).on('value', snapshot =>{
            var preguntas = snapshot.val();
            for(array3 in preguntas){
                database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array3}/tipo`).on('value', snapshot =>{
                    var tipo = snapshot.val();
                    tipoDePregunta.push(tipo);
                });
                database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array3}/pregunta`).on('value', snapshot =>{
                    var tipo = snapshot.val();
                    nombreDePregunta.push(tipo);
                });
            }
        });
        respuestas4(tipoDePregunta,nombreDePregunta,todasLasRespuestas);
        numGraficas(id, tipoDePregunta,nombreDePregunta,todasLasRespuestas);
    }else{
        if(i == 1){
            database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas`).on('value', snapshot =>{
                var preguntas = snapshot.val();
                for(array3 in preguntas){
                    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array3}/tipo`).on('value', snapshot =>{
                        var tipo = snapshot.val();
                        tipoDePregunta.push(tipo);
                    });
                    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array3}/pregunta`).on('value', snapshot =>{
                        var tipo = snapshot.val();
                        nombreDePregunta.push(tipo);
                    });
                }
            });
        }else if(i == cantidadDeUsuarios){
            respuestas4(tipoDePregunta,nombreDePregunta,todasLasRespuestas);
            numGraficas(id,tipoDePregunta,nombreDePregunta, todasLasRespuestas);
        }
    }
}

//mostrar la preguntas abiertas
const respuestas4 = (tipoDePregunta,nombreDePregunta,todasLasRespuestas) =>{
    var htmlpreguntasabiertas = `<div class="container-card2"><h3><b>Preguntas abiertas</b></h3></div>`;
    var i = 0;
    for(let array in nombreDePregunta){
        if(tipoDePregunta[array] == 0){
            htmlpreguntasabiertas = htmlpreguntasabiertas + `<div class="container-preguntas"><p><b>`+ nombreDePregunta[array] +`</b></p>`;
            for(array2 in todasLasRespuestas){
                if(todasLasRespuestas[array2][i] == null || todasLasRespuestas[array2][i] == undefined){
                    i++;
                }
                htmlpreguntasabiertas = htmlpreguntasabiertas + `<p>${todasLasRespuestas[array2][i]}</p>`;
            }
            htmlpreguntasabiertas = htmlpreguntasabiertas + `</div><br>`;
        }
        i++;
        
    }
    var contenido = document.getElementById('card2');
    contenido.innerHTML = htmlpreguntasabiertas;
}

// funciones para que se muestren las graficas 

function numGraficas(id, tipoDePregunta,nombreDePregunta,todasLasRespuestas){ 
    var estructurahtml = ``;
    var i = 1;
    for(let array in nombreDePregunta){
        if(tipoDePregunta[array] == 2 || tipoDePregunta[array] == 3){
            if(i%2 == 1){
                estructurahtml = estructurahtml + 
                `<div class="contenedor__graficas" >
                    <div class="container-cards1">
                        <div class="card">
                            <div class="container-card" >
                                <h3><b>${nombreDePregunta[array]}</b></h3>
                            </div>
                            <div class="grafica">
                            <canvas id="myChart${i}"></canvas>
                            </div>
                        </div>
                    </div>`;
            }else if(i%2 == 0){
                estructurahtml = estructurahtml + 
                `   <div class="container-cards">
                        <div class="card">
                            <div class="container-card">
                                <h3><b>${nombreDePregunta[array]}</b></h3>
                            </div>
                            <div class="grafica">
                                <canvas id="myChart${i}"></canvas>
                            </div>
                        </div>
                    </div>
                </div><br>`;
            }
        }
        i++;
    }
    var contenido = document.getElementById('graficas-pregunta');
    contenido.innerHTML = estructurahtml;

    let arrayOpcionesPreguntas = [];
    var j = 0;
    for(let array in nombreDePregunta){
        database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${j}/opciones`).orderByValue().on('value', snapshot => {
            var opciones = snapshot.val();
            if(opciones == null){
                j++
                database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${j}/opciones`).orderByValue().on('value', snapshot => {
                    var opciones = snapshot.val();
                    var arrayOpciones = [];
                    for(array2 of opciones){
                        if(array2 != null || array2 != undefined){
                            arrayOpciones.push(array2);
                        }
                    }
                    arrayOpcionesPreguntas.push(arrayOpciones);
                });
            }
            var arrayOpciones = [];
            for(array2 of opciones){
                if(array2 != null || array2 != undefined){
                    arrayOpciones.push(array2);
                }
            }
            arrayOpcionesPreguntas.push(arrayOpciones);
        });
        j++;
    }
    
    let datos = [];
    var k = 1;
    for(let array2 in tipoDePregunta){
        let a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0, l=0, m=0;
        for(array in todasLasRespuestas){
            if(tipoDePregunta[array2] == 2){
                let buscar =  arrayOpcionesPreguntas[array2].indexOf(todasLasRespuestas[array][k]);
                if(buscar == 0){
                    a++;
                }else if(buscar == 1){
                    b++;
                }else if(buscar == 2){
                    c++;
                }else if(buscar == 3){
                    d++;
                }else if(buscar == 4){
                    e++;
                }else if(buscar == 5){
                    f++;
                }else if(buscar == 6){
                    g++;
                }else if(buscar == 7){
                    h++;
                }else if(buscar == 8){
                    l++;
                }else if(buscar == 9){
                    m++;
                }
            }
        }
        k++;
        datos.push([a,b,c,d,e,f,g,h,l,m]);
    }

    let arrayTipo3Respuestas = [];
    let arrayNoPregunta = [];
    var k = 1;
    for(let array in tipoDePregunta){
        if(tipoDePregunta[array] == 3){
            for(array2 in todasLasRespuestas){
                let nueva = todasLasRespuestas[array2][k].split(" | ");
                nueva.shift();
                arrayTipo3Respuestas.push(nueva);
                arrayNoPregunta.push(array);
            }
        }
        k++;
    }

    var k = 0;
    var datos2 = [];
    for(let array of arrayNoPregunta){
        let a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0, l=0, m=0;
        for(let array2 in todasLasRespuestas){
            for(let array3 in arrayTipo3Respuestas[k]){
                let buscar =  arrayOpcionesPreguntas[array].indexOf(arrayTipo3Respuestas[k][array3]);
                if(buscar == 0){
                    a++;
                }else if(buscar == 1){
                    b++;
                }else if(buscar == 2){
                    c++;
                }else if(buscar == 3){
                    d++;
                }else if(buscar == 4){
                    e++;
                }else if(buscar == 5){
                    f++;
                }else if(buscar == 6){
                    g++;
                }else if(buscar == 7){
                    h++;
                }else if(buscar == 8){
                    l++;
                }else if(buscar == 9){
                    m++;
                }
            }
            k++;
        }
        datos2.push([a,b,c,d,e,f,g,h,l,m]);
    }

    let inc = 1;
    let pSimples = 0;
    let pMultiples = 0;
    for(array in arrayOpcionesPreguntas){
        if(arrayOpcionesPreguntas[array] != null || arrayOpcionesPreguntas[array] != undefined){
            if(tipoDePregunta[array] == 2){
                var ctx = document.getElementById(`myChart${inc}`).getContext("2d");
                var myChart = new Chart(ctx,{
                    type: "bar",
                    data:{
                        labels:arrayOpcionesPreguntas[array],
                        datasets:[{
                            label:'Preguntas simples',
                            data:datos[pSimples],
                            backgroundColor:[
                            '#37c54b',
                            '#645eff',
                            '#e54345',
                            '#fdcae1'
                            ]
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                          display: true,
                          text: 'Preguntas simples'
                        },
                        tooltips: {
                          mode: 'index',
                          intersect: false
                        },
                        legend: {
                          display: false,
                        },
                        scales: {
                          xAxes: [{
                            ticks: {
                              beginAtZero: true
                            }
                          }]
                        }
                    }
                })
                pSimples++;
            }else if(tipoDePregunta[array] == 3){
                var ctx = document.getElementById(`myChart${inc}`).getContext("2d");
                var myChart = new Chart(ctx,{
                    type: "pie",
                    
                    data:{
                        labels:arrayOpcionesPreguntas[array],
                        datasets:[{
                            label:'Preguntas Multiples',
                            data:datos2[pMultiples],
                            backgroundColor:[
                            '#37c54b',
                            '#645eff',
                            '#e54345',
                            '#fdcae1'
                            ]
                        }]
                    },
                    options: {
                        responsive: true,
                        title: {
                          display: true,
                          text: 'Preguntas Multiples'
                        }
                    }
                })
                pMultiples++;
            }
        }
        inc++;
    }

    tablaExcel(arrayOpcionesPreguntas, id, tipoDePregunta,nombreDePregunta,todasLasRespuestas);
}

// para hacer una tabla para excel
const tablaExcel = (arrayOpcionesPreguntas, id, tipoDePregunta,nombreDePregunta,todasLasRespuestas)=>{

    let arrayTipo3Respuestas = [];
    let arrayNoPregunta = [];
    var k = 1;
    for(let array in tipoDePregunta){
        if(tipoDePregunta[array] == 3){
            for(array2 in todasLasRespuestas){
                let nueva = todasLasRespuestas[array2][k].split(" | ");
                nueva.shift();
                arrayTipo3Respuestas.push(nueva);
                arrayNoPregunta.push(array);
            }
        }
        k++;
    }

    let j = 0;
    let datos = [];
    var k = 1;
    for(let array2 in tipoDePregunta){
        for(array in todasLasRespuestas){
            let a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0, l=0, m=0;
            if(tipoDePregunta[array2] == 2){
                let buscar =  arrayOpcionesPreguntas[array2].indexOf(todasLasRespuestas[array][k]);
                if(buscar == 0){
                    a++;
                }else if(buscar == 1){
                    b++;
                }else if(buscar == 2){
                    c++;
                }else if(buscar == 3){
                    d++;
                }else if(buscar == 4){
                    e++;
                }else if(buscar == 5){
                    f++;
                }else if(buscar == 6){
                    g++;
                }else if(buscar == 7){
                    h++;
                }else if(buscar == 8){
                    l++;
                }else if(buscar == 9){
                    m++;
                }
                datos.push([a,b,c,d,e,f,g,h,l,m]);
            }else if(tipoDePregunta[array2] == 3){
                for(array3 in arrayTipo3Respuestas){
                    let buscar =  arrayOpcionesPreguntas[array2].indexOf(arrayTipo3Respuestas[j][array3]);
                    if(buscar == 0){
                        a++;
                    }else if(buscar == 1){
                        b++;
                    }else if(buscar == 2){
                        c++;
                    }else if(buscar == 3){
                        d++;
                    }else if(buscar == 4){
                        e++;
                    }else if(buscar == 5){
                        f++;
                    }else if(buscar == 6){
                        g++;
                    }else if(buscar == 7){
                        h++;
                    }else if(buscar == 8){
                        l++;
                    }else if(buscar == 9){
                        m++;
                    }
                }
                datos.push([a,b,c,d,e,f,g,h,l,m]);
                j++;
            }else if(tipoDePregunta[array2] == 0){
                datos.push([todasLasRespuestas[array][k]]);
            }
        }
        k++;
    }
    
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/titulo`).on('value', snapshot =>{
        window.tituloDeEncuesta = snapshot.val();
    });
    let arrayTituloDePregunta = [];
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas`).on('value', snapshot =>{
        let preguntas = snapshot.val();
        for(let array in preguntas){
            database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array}/pregunta`).on('value', snapshot =>{
                let titulo = snapshot.val();
                arrayTituloDePregunta.push(titulo);
            });
        }
    });

    let arrayNombreDeUsuarios = [];
    database.ref(`/Usuarios/${id}/quien_responde/${window.num}`).on('value', snapshot => {
        var usuariosPorEncuesta = snapshot.val();
        for(array in usuariosPorEncuesta){
            if(array != 'n_quien'){
                database.ref(`/Usuarios/${id}/quien_responde/${window.num}/${array}`).on('value', snapshot => {
                    var idUsuario = snapshot.val();
                    database.ref(`/Usuarios/${idUsuario}/nombre`).on('value', snapshot => {
                        var nombreUsuario = snapshot.val();
                        arrayNombreDeUsuarios.push(nombreUsuario);
                    });
                });
            }
        }
    });


    let contador = 0, contador3 = 0;
    let tablaParaExcel = `<tr><th colspan="10">${window.tituloDeEncuesta}</th></tr>`;
    for(let array of arrayTituloDePregunta){
        let  contador2 = 1;
        tablaParaExcel = tablaParaExcel + `<tr><td colspan="10" style="color: red;">${array}</td></tr>
        <tr><td>No</td><td>Nombre</td>`;
        for(let array2 of arrayOpcionesPreguntas[contador]){
            tablaParaExcel = tablaParaExcel + `<td>${array2}</td>`;
        }
        tablaParaExcel = tablaParaExcel + `</tr>`;
        for(let array2 in arrayNombreDeUsuarios){
            tablaParaExcel = tablaParaExcel + `<tr><td>${contador2}</td><td>${arrayNombreDeUsuarios[array2]}</td>`;
            for(let array3 in arrayOpcionesPreguntas[contador]){
                tablaParaExcel = tablaParaExcel +`<td>${datos[contador3][array3]}</td>`;
            }
            tablaParaExcel = tablaParaExcel + `</tr>`;
            contador3++;
            contador2++;
        }
        contador++;
    }

    var contenido = document.getElementById('tblData');
    contenido.innerHTML = tablaParaExcel;
}

// numero de encuesta
function encuestas(num) {
    window.num = num;
}

// cerrar sesión
function cerrar() {
    firebase.auth().signOut()
        .then(function () {
            console.log('Cerrando...');
            //location.reload();
            window.location.href = 'index.html';
        })
        .catch(function (error) {
            alert(error);
        });
}