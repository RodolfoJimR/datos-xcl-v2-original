var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };

// Initialize Firebase
if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
  }
firebase.analytics();

// Obtener id de usuario
firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        var uid = user.uid;
        preguntasFormularios(uid);
        respuestasFormularios(uid);
    } else {
    }
});

// Función para generar color aleatorio en gráficas
function randomColor(){
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
// Obtener en el eje X el número de los formularios, en el eje Y 
function preguntasFormularios(uid){
    let graficaX = [];
    let graficaY = [];
    database.ref(`Usuarios/${uid}/Formularios/`).once("value", function(snapshot){
        snapshot.forEach(function(childSnapshot){
            let dataX = childSnapshot.key;
            graficaX.push(dataX);
            database.ref(`Usuarios/${uid}/Formularios/${dataX}/preguntas/`).limitToLast(1).once("value", function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    let dataY = childSnapshot.val().id;
                    graficaY.push(dataY);
                });
            });
            
        });
        const ctx = document.getElementById('myChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: graficaX,
                datasets: [{
                    label: 'Preguntas',
                    data: graficaY,
                    backgroundColor:randomColor(),
                    borderColor: [
                        '#7a7974'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                        barPercentage: 1
                    }
                }
            }
        });
    });
    
}
