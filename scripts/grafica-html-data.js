var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };

// Initialize Firebase
if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
  }
firebase.analytics();

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        let uid = user.uid;
        datos__firebase(uid);
    } else {
        // User is signed out
        // ...
    }
});


const database = firebase.database();

// datos que se van a estraer de la base de datos
function datos__firebase(id) {

    // mostrar nombre del usuario
    database.ref(`/Usuarios/${id}/`).orderByValue().on('value', snapshot => {
        var nombre_usuario = snapshot.val().nombre;
        var correo_usuario = snapshot.val().usuario;
        var contenido2 = document.getElementById('usuario-nombre');
        var contenido = document.getElementById("nombre_usuario");
        contenido.innerHTML = `<br><div class="datos-usuario"><span class="nombre-usuario">${nombre_usuario}</span><span class = "correo-usuario">${correo_usuario}</span></div>`;
        if(contenido2){
            contenido2.innerHTML = `<h2>${nombre_usuario}</h2><br>`;
        }
    });

    // para mostrar las encuestas cargadas con el numero de usuarios por encuesta
    database.ref((`/Usuarios/${id}/Formularios`)).orderByValue().on('value', snapshot => {
        let nombre_usuario2 = snapshot.val();
        let arrayUsuarios = [];
        for(array2 in nombre_usuario2){
            database.ref(`/Usuarios/${id}/Formularios/${array2}/id_usuario_origen`).on('value', snapshot => {
                var formularios = snapshot.val();
                arrayUsuarios.push(formularios);
            });
        }
        for(array3 in arrayUsuarios){
            if(arrayUsuarios[array3] == id){
                database.ref(`/Usuarios/${id}/quien_responde/${parseInt(array3)+1}`).on('value', snapshot => {
                    var formularios = snapshot.val();
                    var i=0;
                    for(array4 in formularios){
                        if(array4 != 'n_quien'){
                            i++;
                        }
                    }
                    tercera_funcion(i);
                });
            }
        }
    });
    
    //mostrar las encuestas realizadas
    database.ref((`/Usuarios/${id}/Respuestas`)).on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        var estructura__tabla2 = ``;
        var encuesta = ``;
        var i = 0;
        for(array in nombre_usuario2){
            var estructura__tabla2 = estructura__tabla2 + `<tr><td>Encuesta ${array}</td></tr>`;

            var encuesta = encuesta + `<li><form action="encuestas-realizadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${array}"></p><input type="submit" value="Encuesta ${array}" /></form></li>`;

            i++;
        }
        var estructura__tabla2 = `<tr title="Encuestas contestadas en la Aplicación"><th>Realizadas: ${i}</th></tr>${estructura__tabla2}`;

        var contenido = document.getElementById('primera__tabla');
        contenido.innerHTML = estructura__tabla2;

        var numero__encuestas__realizadas = `<img src="imagenes/flecha-hacia-abajo-para-navegar.png" class="caret">Encuestas Realizadas: ${i}`;

        var contenido = document.getElementById('encuestas__realizadas');
        contenido.innerHTML = numero__encuestas__realizadas;
        var contenido = document.getElementById('numero__realizadas');
        contenido.innerHTML = encuesta;
    });

    window.id = id;
}
var j = 0;
let estructura__tabla = `<tr title="Encuestas Creadas en la Aplicación"><th>Cargadas</th></tr>`;
let encuesta = ``;
function tercera_funcion(i){
    j++;
    estructura__tabla = estructura__tabla + `<tr><td>Encuesta ${j} &nbsp;&nbsp;&nbsp;&nbsp;: ${i} Usuario</td></tr>`;
    encuesta = encuesta + `<li><form action="encuestas-cargadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${j}"></p><input type="submit" value="Encuesta ${j} : ${i} Usuarios" /></form></li>`;
    var contenido = document.getElementById('primera__tabla2');
    contenido.innerHTML = estructura__tabla;
    var contenido2 = document.getElementById('encuestas__cargadas');
    contenido2.innerHTML = encuesta;
}



// cerrar sesión
function cerrar() {
    firebase.auth().signOut()
        .then(function () {
            console.log('Cerrando...');
            //location.reload();
            window.location.href = 'index.html';
        })
        .catch(function (error) {
            alert(error);
        });
}