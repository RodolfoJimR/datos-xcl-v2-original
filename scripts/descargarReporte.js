const descargar = () =>{
	html2pdf(document.getElementById('reportPage'), {
		margin:       10,
		filename:     'reporte.pdf',
		image:        { type: 'jpeg', quality: 1.98 },
		html2canvas:  { scale: 3, logging: true, dpi: 200, letterRendering: true},
		jsPDF:        { unit: 'mm', format: 'a3', orientation: 'p' }
	});
	html2pdf().set(opt).from(element).save();
}
 function Exportar(){
        var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        var table = 'tblData';
        var name = 'nombre_hoja_calculo';

        if (!table.nodeType) table = document.getElementById(table)
         var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
         window.location.href = uri + base64(format(template, ctx))
}
function ExportarUsuarios(){
       var uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
       , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
       , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

       var table = 'tabla-usuarios';
       var name = 'nombre_hoja_calculo';

       if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
}
function ExportarPreguntas(){
       var uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
       , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
       , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

       var table = 'tabla-preguntas';
       var name = 'nombre_hoja_calculo';

       if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
}
function ExportarRespuestas(){
       var uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
       , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
       , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

       var table = 'tabla-respuestas';
       var name = 'nombre_hoja_calculo';

       if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        window.location.href = uri + base64(format(template, ctx))
}
// function exportTableToExcel(tableID, filename = ''){
//     var downloadLink;
//     var dataType = 'application/vnd.ms-excel/charset=utf-8';
//     var tableSelect = document.getElementById(tableID);
//     var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

//     // Specify file name
//     filename = filename?filename+'.xls':'excel_data.xls';

//     // Create download link element
//     downloadLink = document.createElement("a");

//     document.body.appendChild(downloadLink);

//     if(navigator.msSaveOrOpenBlob){
//         var blob = new Blob(['ufeff', tableHTML], {
//             type: dataType
//         });
//         navigator.msSaveOrOpenBlob( blob, filename);
//     }else{
//         // Create a link to the file
//         downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

//         // Setting the file name
//         downloadLink.download = filename;

//         //triggering the function
//         downloadLink.click();
//     }
// }

// var tableToExcel = (function() {
//   var uri = 'data:application/vnd.ms-excel;base64,'
//     , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
//     , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
//     , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
//   return function(table, name) {
//     if (!table.nodeType) table = document.getElementById(table)
//     var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
//     window.location.href = uri + base64(format(template, ctx))
//   }
// })()


// // html2pdf
// var element = document.getElementById('encuesta-por-nombre');
// html2pdf(document.getElementById('encuesta-por-nombre');, {
// 	margin:       10,
// 	filename:     'reporte.pdf',
// 	image:        { type: 'jpeg', quality: 0.98 },
// 	html2canvas:  { scale: 2, logging: true, dpi: 192, letterRendering: true },
// 	jsPDF:        { unit: 'mm', format: 'a4', orientation: 'l' }
// });

// html2pdf().set(opt).from(element).save();


// html2canvas(document.getElementById('encuesta-por-nombre'), {
// 	imageTimeout: 15000,
// 	loggin: true,
// 	useCORS: false,
// 	scale: 3,
// 	onrendered: function(canvas) {
// 		var imgData = canvas.toDataURL('image/png');
// 		imagen = imgData;
// 		doc.addImage(imagen, 'PNG', -25, 10);
// 	}
// });

// let y = 100;
// let x = 40;
// let contador = 0;
// $(".container-card2").each(function(index) {
// 	html2canvas($(this), {
// 		imageTimeout: 15000,
// 		loggin: true,
// 		useCORS: false,
// 		scale: 3,
// 		onrendered: function(canvas) {
// 			if(contador == 2){
// 				doc.addPage();
// 			}   
// 			var imgData = canvas.toDataURL('image/png');
// 			doc.addImage(imgData, 'PNG', x, y, 120,70);
// 			y= y + 75;
// 			contador++;
// 		}
// 	});
// });
// doc.addPage();
// html2canvas(document.getElementById('card2'), {
// 	imageTimeout: 15000,
// 	loggin: true,
// 	useCORS: false,
// 	onrendered: function(canvas) {         
// 		var imgData = canvas.toDataURL('image/png');
// 		doc.addImage(imgData, 'PNG', 10, 10);
// 		doc.save('reporte.pdf');
// 	}
// });