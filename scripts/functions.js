var firebaseConfig = {
  apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
  authDomain: "dynamic-forms-87996.firebaseapp.com",
  databaseURL: "https://dynamic-forms-87996.firebaseio.com",
  projectId: "dynamic-forms-87996",
  storageBucket: "dynamic-forms-87996.appspot.com",
  messagingSenderId: "831911899167",
  appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
  measurementId: "G-Z1NL5W726N"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

function ingreso(){
    var usuario = document.getElementById('usuario').value;
    var password = document.getElementById('password').value;
  
    firebase.auth().signInWithEmailAndPassword(usuario, password)
    .then(function(){
        window.location.href = 'dashboard.html';
    })
    .catch(function(error){
      var errorCode = error.code;
      var errorMessage = error.message;
  
      console.log(errorCode);
      alert(errorMessage); 
    });
  }
