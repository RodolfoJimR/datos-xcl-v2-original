var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        var uid = user.uid;
        datos__firebase(uid);
    } else {
        // User is signed out
        // ...
    }
});

const database = firebase.database();

// datos que se van a estraer de la base de datos
function datos__firebase(id) {
    // mostrar nombre del usuario
    database.ref(`/Usuarios/${id}/nombre`).orderByValue().on('value', snapshot => {
        var nombre_usuario = snapshot.val()
        var contenido = document.getElementById('nombre_usuario');
        contenido.innerHTML = `<br><div class="datos-usuario"><span class="nombre-usuario">${nombre_usuario}</span></div>`;
    });

    // mostrar correo del usuario

    database.ref(`/Usuarios/${id}/usuario`).orderByValue().on('value', snapshot => {
        var correo_usuario = snapshot.val();
        var contenido3 = document.getElementById('correo_usuario');
        contenido3.innerHTML = `<div class="datos-usuario"><span class="correo-usuario">${correo_usuario}</span></div>`;
    });

    // para mostrar las encuestas cargadas con el numero de usuarios por encuesta
    database.ref((`/Usuarios/${id}/Formularios`)).orderByValue().on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        nombre_usuario2 = nombre_usuario2.length;
        primera_funcion(id, nombre_usuario2);
    });

    //mostrar las encuestas realizadas
    database.ref((`/Usuarios/${id}/Respuestas`)).on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        var encuesta = ``;
        var i = 0;
        for(array in nombre_usuario2){

            var encuesta = encuesta + `<li><form action="encuestas-realizadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${array}"></p><input type="submit" value="Encuesta ${array}" /></form></li>`;

            i++;
        }
        var numero__encuestas__realizadas = `<img src="imagenes/flecha-hacia-abajo-para-navegar.png" class="caret">Encuestas Realizadas: ${i}`;

        var contenido = document.getElementById('encuestas__realizadas');
        contenido.innerHTML = numero__encuestas__realizadas;
        var contenido = document.getElementById('numero__realizadas');
        contenido.innerHTML = encuesta;
    });

    //mostar el titulo de la encuesta
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/titulo`).on('value', snapshot => {
        var titulo = snapshot.val();
        var contenido = document.getElementById('titulo__encuesta');
        contenido.innerHTML = titulo;
    });

    //mostrar la descripcion de la encuesta
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/descripcion`).on('value', snapshot => {
        var descripcion = snapshot.val();
        var contenido = document.getElementById('descripcion__encuesta');
        contenido.innerHTML = descripcion;
    });

    //mostrar las preguntas abiertas
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas`).on('value', snapshot => {   
        var descripcion = snapshot.val();
        var i = 0;
        var htmltexto = `<div class="container-card2">
                            <h3><b>Preguntas abiertas</b></h3>
                        </div>`;
        for(let array in descripcion){
            database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array}/tipo`).on('value', snapshot => {   
                var tipo = snapshot.val();
                if(tipo == 0){
                    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array}/pregunta`).on('value', snapshot => {   
                        var tituloPregunta = snapshot.val();
                        database.ref(`/Usuarios/${id}/Respuestas/${window.num}/${array}`).on('value', snapshot => {   
                            var respuesta = snapshot.val();
                            htmltexto = htmltexto +`<div class="container-preguntas"><p><b>${tituloPregunta}</b></p><p>${respuesta}</p></div><br>`;
                            var contenido = document.getElementById('card2');
                            contenido.innerHTML = htmltexto;
                        });
                    });
                    
                }
            });
        }
        
    });
    
    //envia a una funcion las respuestas para que haci puedan ser graficadas 
    database.ref(`/Usuarios/${id}/Respuestas/${window.num}`).on('value', snapshot => {
        let respuestas = snapshot.val();
        mostrarGraficas(id, respuestas);
    });
}

// funcion para mostrar las encustas cargadas en el menu 
var arrayUsuarios = [];
function primera_funcion(id, cantidad) {
    var arraycantidadUsuarios = [];
    var j = 0;
    for (var i = 1; i < cantidad; i++) {
        database.ref('/Usuarios/' + id + '/Formularios/' + i + '/id_usuario_origen').on('value', snapshot => {
            var formularios = snapshot.val();
            arrayUsuarios.push(formularios);
        });
    }
    segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
}

function segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j) {
    database.ref('/Usuarios/' + id + '/quien_responde/' + j).on('value', snapshot => {
        usuarios_encuesta = snapshot.val();
        if (usuarios_encuesta != null) {
            var l = 0;
            for (var k = 0; k < 50; k++) {
                if (usuarios_encuesta[k] != undefined) {
                    l++;
                }
            }
        } else {
            l = 0;
        }
        arraycantidadUsuarios.push(l);
        tercera_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
    });
}

function tercera_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j) {
    var encuesta = ``;
    j++;
    if (j < cantidad) {
        segunda_funcion(id, cantidad, arrayUsuarios, arraycantidadUsuarios, j);
    } else {
        if (arraycantidadUsuarios.length != arrayUsuarios.length) {
            arrayUsuarios.unshift(null);
        }
        for (var i = 0; i < cantidad; i++) {
            if (arrayUsuarios[i] == id) {
                encuesta = encuesta + `<li><form action="encuestas-cargadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="` + i + `"></p><input type="submit" value="Encuesta ` + i + ` : ` + arraycantidadUsuarios[i] + ` Usuarios" /></form></li>`;
            }
        }
        var contenido2 = document.getElementById('encuestas__cargadas');
        contenido2.innerHTML = encuesta;
    }
}

// numero de encuesta
function encuestas(num) {
    window.num = num;
}

// graficas
const mostrarGraficas = (id, respuestas)=>{
    let cantidadDePreguntas = respuestas.length;
    var arrayTipoDePregunta = [];
    let arrayOpciones = [];
    for(let i=0;i<cantidadDePreguntas;i++){
        database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${i}/tipo`).on('value', snapshot => {
            let respuestas = snapshot.val();
            if(respuestas != null || respuestas != undefined){
                arrayTipoDePregunta.push(respuestas);
            }
        });
    }

    for(let i=0;i<cantidadDePreguntas;i++){
        database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${i}/opciones`).on('value', snapshot => {
            let respuestas = snapshot.val();
            if(respuestas != null || respuestas != undefined){
                arrayOpciones.push(respuestas);
            }
        });
    }
    
    var k = 0;
    for(let array in arrayOpciones){
        if(arrayOpciones[array][0] != null || arrayOpciones[array][0] != undefined){
            arrayOpciones[array].unshift('');
        }
    }
    //Preguntas simples
    var a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0;
    var i=0;
    var j=1;
    var arrayCantidadPorRespuestas = [];
    for(let array of arrayTipoDePregunta){
        if(array == 2){
            if(respuestas[j] != null || respuestas[j] != undefined){
                let buscar =  arrayOpciones[i].indexOf(respuestas[j]);
                if(buscar == 1){
                    a++;
                }else if(buscar == 2){
                    b++;
                }else if(buscar == 3){
                    c++;
                }else if(buscar == 4){
                    d++;
                }else if(buscar == 5){
                    e++;
                }else if(buscar == 6){
                    f++;
                }else if(buscar == 7){
                    g++;
                }else if(buscar == 8){
                    h++;
                }
            }
        }
        i++;
        j++;
    }
    arrayCantidadPorRespuestas.push(a,b,c,d,e,f,g,h);
    //preguntas multiples
  
    var i=0;
    var j=1;
    let arrayTipo3Respuestas = [];
    for(let array in arrayTipoDePregunta){
        if(arrayTipoDePregunta[array] == 3){
            let nueva = respuestas[j].split(" | ");
            nueva.shift();
            arrayTipo3Respuestas.push(nueva);
        }
        i++;
        j++;      
    }
    
    var a=0, b=0, c=0, d=0,e=0, f=0, g=0, h=0;
    var i=0;
    var j=0;
    var arrayCantidadPorRespuestas2 = [];
    for(let array in arrayTipoDePregunta){
        if(arrayTipoDePregunta[array] == 3){
            for(array2 in arrayTipo3Respuestas[j]){
                let buscar =  arrayOpciones[i].indexOf(arrayTipo3Respuestas[j][array2]);
                if(buscar == 1){
                    a++;
                }else if(buscar == 2){
                    b++;
                }else if(buscar == 3){
                    c++;
                }else if(buscar == 4){
                    d++;
                }
            }
            j++;
        }
        i++;
    }
    arrayCantidadPorRespuestas2.push(a,b,c,d,e,f,g,h);
    // chart.js graficas
    let result = arrayTipoDePregunta.filter((item,index)=>{
        return arrayTipoDePregunta.indexOf(item) === index;
    })
    var i=0;
    while(i < result.length){
        if(result[i] !=null && result[i] != undefined){
            if(result[i] == 2){
                var contenido = document.getElementById('myChart-grafica');
                contenido.innerHTML = (`<div class="container-card">
                                            <h3><b>Preguntas simples</b></h3>
                                        </div>
                                        <div  class="grafica">
                                            <canvas id="myChart"></canvas>
                                        </div>`);
                var ctx = document.getElementById("myChart").getContext("2d");
                var myChart = new Chart(ctx,{
                    type: "bar",
                    data:{
                        labels:['A','B','C','D','E','F','G','H'],
                        datasets:[{
                            label:'Preguntas simples',
                            data:arrayCantidadPorRespuestas,
                            backgroundColor:[
                            'rgb(20,248,149,97)',
                            '#84b6f4',
                            '#ff6961',
                            '#fdcae1',
                            'rgb(20,248,149,97)',
                            '#84b6f4',
                            '#ff6961',
                            '#fdcae1'
                            ]
                        }]
                    }
                })
            }else if(result[i] == 3){
                var contenido = document.getElementById('myChart2-grafica');
                contenido.innerHTML = (`<div class="container-card">
                                            <h3><b>Preguntas multiples</b></h3>
                                        </div>
                                        <div  class="grafica">
                                            <canvas id="myChart2"></canvas>
                                        </div>`);
                var ctx2 = document.getElementById("myChart2").getContext("2d");
                var myChart2 = new Chart(ctx2,{
                    type: "pie",
                    data:{
                        labels:['A','B','C','D'],
                        datasets:[{
                            label:'Preguntas multiples',
                            data:arrayCantidadPorRespuestas2,
                            backgroundColor:[
                            'rgb(20,248,149,97)',
                            '#84b6f4',
                            '#ff6961',
                            '#fdcae1'
                            ]
                        }]
                    }
                })
            }
        }
        i++;
    }
    tablaExcel(id, respuestas, arrayTipoDePregunta);
}

// para hacer una tabla para excel
const tablaExcel = (id, todasLasRespuestas, tipoDePregunta)=>{

    database.ref(`/Usuarios/${id}/Formularios/${window.num}/titulo`).on('value', snapshot => {
        window.tituloDeEncuesta = snapshot.val();
    });

    let arrayTituloDePregunta = [];
    database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas`).on('value', snapshot =>{
        let preguntas = snapshot.val();
        for(let array in preguntas){
            database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${array}/pregunta`).on('value', snapshot =>{
                let titulo = snapshot.val();
                arrayTituloDePregunta.push(titulo);
            });
        }
    });

    let arrayOpcionesPreguntas = [];
    var j = 0;
    for(let array in arrayTituloDePregunta){
        database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${j}/opciones`).orderByValue().on('value', snapshot => {
            var opciones = snapshot.val();
            if(opciones == null){
                j++
                database.ref(`/Usuarios/${id}/Formularios/${window.num}/preguntas/${j}/opciones`).orderByValue().on('value', snapshot => {
                    var opciones = snapshot.val();
                    var arrayOpciones = [];
                    for(array2 of opciones){
                        if(array2 != null || array2 != undefined){
                            arrayOpciones.push(array2);
                        }
                    }
                    arrayOpcionesPreguntas.push(arrayOpciones);
                });
            }
            var arrayOpciones = [];
            for(array2 of opciones){
                if(array2 != null || array2 != undefined){
                    arrayOpciones.push(array2);
                }
            }
            arrayOpcionesPreguntas.push(arrayOpciones);
        });
        j++;
    }

    database.ref(`/Usuarios/${id}/nombre`).orderByValue().on('value', snapshot => {
        window.nombre_usuario = snapshot.val();
    });

    let arrayTipo3Respuestas = [];
    var k = 1;
    for(let array in tipoDePregunta){
        if(tipoDePregunta[array] == 3){
            let nueva = todasLasRespuestas[k].split(" | ");
            nueva.shift();
            arrayTipo3Respuestas.push(nueva);
        }
        k++;
    }

    let datos = [];
    var k = 1;
    var j = 0;
    for(let array in tipoDePregunta){
        let a=0, b=0, c=0, d=0, e=0, f=0, g=0, h=0, l=0, m=0;
        if(tipoDePregunta[array] == 2){
            let buscar =  arrayOpcionesPreguntas[array].indexOf(todasLasRespuestas[k]);
            if(buscar == 0){
                    a++;
                }else if(buscar == 1){
                    b++;
                }else if(buscar == 2){
                    c++;
                }else if(buscar == 3){
                    d++;
                }else if(buscar == 4){
                    e++;
                }else if(buscar == 5){
                    f++;
                }else if(buscar == 6){
                    g++;
                }else if(buscar == 7){
                    h++;
                }else if(buscar == 8){
                    l++;
                }else if(buscar == 9){
                    m++;
                }
                datos.push([a,b,c,d,e,f,g,h,l,m]);
        }else if(tipoDePregunta[array] == 3){
            for(let array2 in arrayTipo3Respuestas[j]){
                let buscar =  arrayOpcionesPreguntas[array].indexOf(arrayTipo3Respuestas[j][array2]);
                console.log(arrayTipo3Respuestas[j][array2]);
                if(buscar == 0){
                    a++;
                }else if(buscar == 1){
                    b++;
                }else if(buscar == 2){
                    c++;
                }else if(buscar == 3){
                    d++;
                }else if(buscar == 4){
                    e++;
                }else if(buscar == 5){
                    f++;
                }else if(buscar == 6){
                    g++;
                }else if(buscar == 7){
                    h++;
                }else if(buscar == 8){
                    l++;
                }else if(buscar == 9){
                    m++;
                }    
            }
            datos.push([a,b,c,d,e,f,g,h,l,m]);
            j++;
        }else if(tipoDePregunta[array] == 0){
            datos.push([todasLasRespuestas[k]]);
        }
        k++; 
    }

    let contador = 0;
    let tablaParaExcel = `<tr><th colspan="10">${window.tituloDeEncuesta}</th></tr>`;
    for(let array of arrayTituloDePregunta){
        tablaParaExcel = tablaParaExcel + `<tr><td colspan="10" style="color: red;">${array}</td></tr>
        <tr><td>No</td><td>Nombre</td>`;
        for(let array2 of arrayOpcionesPreguntas[contador]){
            tablaParaExcel = tablaParaExcel + `<td>${array2}</td>`;
        }
        tablaParaExcel = tablaParaExcel + `</tr>`;
        tablaParaExcel = tablaParaExcel + `<tr><td>1</td><td>${window.nombre_usuario}</td>`;
        for(let array3 in arrayOpcionesPreguntas[contador]){
            tablaParaExcel = tablaParaExcel +`<td>${datos[contador][array3]}</td>`;
        }
        tablaParaExcel = tablaParaExcel + `</tr>`;
        contador++;
    }

    var contenido = document.getElementById('tblData');
    contenido.innerHTML = tablaParaExcel;
}

// cerrar sesión
function cerrar() {
    firebase.auth().signOut()
        .then(function () {
            console.log('Cerrando...');
            //location.reload();
            window.location.href = 'index.html';
        })
        .catch(function (error) {
            alert(error);
        });
}