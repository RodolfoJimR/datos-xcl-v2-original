var firebaseConfig = {
    apiKey: "AIzaSyCYMUiaE3ZzHSnZc2NYeShPFF1PNlT4uUs",
    authDomain: "dynamic-forms-87996.firebaseapp.com",
    databaseURL: "https://dynamic-forms-87996.firebaseio.com",
    projectId: "dynamic-forms-87996",
    storageBucket: "dynamic-forms-87996.appspot.com",
    messagingSenderId: "831911899167",
    appId: "1:831911899167:web:7ea7b4ce9ba558b6a0a52a",
    measurementId: "G-Z1NL5W726N"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        let uid = user.uid;
        datos__firebase(uid);
    } else {
        // User is signed out
        // ...
    }
});

const database = firebase.database();

// datos que se van a estraer de la base de datos
function datos__firebase(id) {
 
    // mostrar nombre del usuario

    database.ref(`/Usuarios/${id}/`).orderByValue().on('value', snapshot => {
        var nombre_usuario = snapshot.val().nombre;
        var correo_usuario = snapshot.val().usuario;
        var contenido2 = document.getElementById('usuario-nombre');
        var contenido = document.getElementById("nombre_usuario");
        contenido.innerHTML = `<br><div class="datos-usuario"><span class="nombre-usuario">${nombre_usuario}</span><span class = "correo-usuario">${correo_usuario}</span></div>`;
        if(contenido2){
            contenido2.innerHTML = `<h2>${nombre_usuario}</h2><br>`;
        }
    });

    // mostrar correo del usuario

    database.ref(`/Usuarios/${id}/usuario`).orderByValue().on('value', snapshot => {
        var correo_usuario = snapshot.val();
        var contenido3 = document.getElementById('correo-usuario');
        contenido3.innerHTML = `<div class="datos-usuario"><span class="correo-usuario">${correo_usuario}</span></div>`;
    });

    // para mostrar las encuestas cargadas con el numero de usuarios por encuesta
    database.ref((`/Usuarios/${id}/Formularios`)).orderByValue().on('value', snapshot => {
        let nombre_usuario2 = snapshot.val();
        let arrayUsuarios = [];
        for(array2 in nombre_usuario2){
            database.ref(`/Usuarios/${id}/Formularios/${array2}/id_usuario_origen`).on('value', snapshot => {
                var formularios = snapshot.val();
                arrayUsuarios.push(formularios);
            });
        }
        for(array3 in arrayUsuarios){
            if(arrayUsuarios[array3] == id){
                database.ref(`/Usuarios/${id}/quien_responde/${parseInt(array3)+1}`).on('value', snapshot => {
                    var formularios = snapshot.val();
                    var i=0;
                    for(array4 in formularios){
                        if(array4 != 'n_quien'){
                            i++;
                        }
                    }
                    tercera_funcion(i);
                });
            }
        }
    });
    
    //mostrar las encuestas realizadas
    database.ref((`/Usuarios/${id}/Respuestas`)).on('value', snapshot => {
        var nombre_usuario2 = snapshot.val();
        var estructura__tabla2 = ``;
        var encuesta = ``;
        var i = 0;
        for(array in nombre_usuario2){
            var estructura__tabla2 = estructura__tabla2 + `<tr><td>Encuesta ${array}</td></tr>`;

            var encuesta = encuesta + `<li><form action="encuestas-realizadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${array}"></p><input type="submit" value="Encuesta ${array}" /></form></li>`;

            i++;
        }
        var estructura__tabla2 = `<tr title="Encuestas contestadas en la Aplicación"><th>Realizadas: ${i}</th></tr>${estructura__tabla2}`;

        var contenido = document.getElementById('primera__tabla');
        contenido.innerHTML = estructura__tabla2;

        var numero__encuestas__realizadas = `<img src="imagenes/flecha-hacia-abajo-para-navegar.png" class="caret">Encuestas Realizadas: ${i}`;

        var contenido = document.getElementById('encuestas__realizadas');
        contenido.innerHTML = numero__encuestas__realizadas;
        var contenido = document.getElementById('numero__realizadas');
        contenido.innerHTML = encuesta;
    });
    // Rodolfo
    // Obtener datos de formulario (preguntas.html)
    let tablaFormularios = document.getElementById("tabla-preguntas");
    if(tablaFormularios){
        database.ref(`Usuarios/${id}/Formularios`).once("value", function(todosFormularios){
            let rowFormulario = '';
            todosFormularios.forEach(
                function(formularioActual){
                let dataForm = formularioActual.val();
                rowFormulario += '<tr>';
                rowFormulario += '<td>' + dataForm.id + '</td>';
                rowFormulario += '<td>' + dataForm.titulo + '</td>';
                rowFormulario += '<td>' + dataForm.descripcion + '</td>';
                rowFormulario += '</tr>'
            });
            tablaFormularios.innerHTML += rowFormulario;
        });
    }

    //Obtener valores en select-box (preguntas.html)
    var optionRespuestas = document.getElementById("select-box");
    if(optionRespuestas){
        database.ref((`/Usuarios/${id}/Formularios`)).on('value', snapshot => {
            var formOption = snapshot.val();
            var i = 0;
            for(let array in formOption){
                optionRespuestas.innerHTML += `<option value="${array}">${array}</option>`;
                i++;
            }
        });
    }

    //Boton para cargar preguntas de formulario
    let buttonEl = document.getElementById("buscar-form");
    let selectBox = document.getElementById("select-box");
    let numForm;
    if(buttonEl){
        buttonEl.addEventListener("click", function(){
            numForm = selectBox.value;
            let tablebody = document.getElementById("preguntas-form")
            var count = tablebody.rows.length;
            for (var i = count -1; i > 0; i--) {
                tablebody.deleteRow(i);
            }
            obtenerPreguntas();
        });
    }

    //Cargar preguntas de formulario
    function obtenerPreguntas(){
        var tablePregunta = document.getElementById("preguntas-form");
        if(tablePregunta){
            database.ref(`Usuarios/${id}/Formularios/${numForm}/preguntas`).once("value", function(datosPregunta){
                let rowPreguntas = '';
                datosPregunta.forEach(
                    function (preguntaActual){
                        let dataPregunta = preguntaActual.val();
                        rowPreguntas += '<tr>';
                        rowPreguntas += '<td>' + dataPregunta.id + '</td>';
                        rowPreguntas += '<td>' + dataPregunta.pregunta + '</td>';
                        rowPreguntas += '<td>' + dataPregunta.tipo + '</td>';
                        rowPreguntas += '</tr>';
                    }
                );
                tablePregunta.innerHTML += rowPreguntas
            })
        }
    }

    // Obtener valores en select-box (respuestas.html)
    database.ref((`/Usuarios/${id}/Respuestas`)).on('value', snapshot => {
        var formOption = snapshot.val();
        var optionRespuestas = document.getElementById("respuestas");
        var i = 0;
        for(let array in formOption){
            optionRespuestas.innerHTML += `<option value="${array}">${array}</option>`;
            i++;
        }
    });

    let selectBoxResp = document.getElementById("respuestas");
    let buttonElResp = document.getElementById("buscar-form");
    let numRespuesta;
    if(buttonElResp){
        buttonElResp.addEventListener("click", function(){
            numRespuesta = selectBoxResp.value;
            let tablebody = document.getElementById("respuestas-form")
            var count = tablebody.rows.length;
            for (var i = count -1; i >= 0; i--) {
                tablebody.deleteRow(i);
            }
            obtenerRespuestas();
        });
    }
    //Cargar respuestas en respuestas.html
    function obtenerRespuestas(){
        database.ref(`Usuarios/${id}/Respuestas/${numRespuesta}`).once("value", function(infoRespuesta){
            infoRespuesta.forEach(
                function (respuestaActual){
                    var datosRespuesta = respuestaActual.val();
                    tablaRespuesta(datosRespuesta);
                }
            );
        })
    }
    function tablaRespuesta(datosRespuesta){
        var tbodyrespuesta = document.getElementById("respuestas-form");
        var trowrespuesta = document.createElement("tr");
        var tdrespuesta = document.createElement("td");

        tdrespuesta.innerHTML = datosRespuesta;

        trowrespuesta.appendChild(tdrespuesta);

        tbodyrespuesta.appendChild(trowrespuesta);
    }
    

    window.id = id;
    exportarDatos();
}
// Tabla de usuarios
let tablaUsuarios = document.getElementById("tabla-usuarios");
if(tablaUsuarios){
    database.ref("Usuarios").once("value", function(snapshot){
        let rowUsuario = '';
        // Nombre, usuario, n_form
        snapshot.forEach(function(childSnapshot){
            let data = childSnapshot.val();
            rowUsuario += `<tr>`;
            rowUsuario += '<td>' + data.nombre + '</td>';
            rowUsuario += '<td>' + data.usuario + '</td>';
            rowUsuario += '<td>' + data.n_form + '</td>';
            rowUsuario += '</tr>';
        });
        tablaUsuarios.innerHTML += rowUsuario;
    });
}


var j = 0;
let estructura__tabla = `<tr title="Encuestas Creadas en la Aplicación"><th>Cargadas</th></tr>`;
let encuesta = ``;
function tercera_funcion(i){
    j++;
    estructura__tabla = estructura__tabla + `<tr><td>Encuesta ${j} &nbsp;&nbsp;&nbsp;&nbsp;: ${i} Usuario</td></tr>`;
    encuesta = encuesta + `<li><form action="encuestas-cargadas.php" method="POST"><p class="hiding"><label  for="fname">First name:</label>
                <input type="text" id="fname" name="fname" value="${j}"></p><input type="submit" value="Encuesta ${j} : ${i} Usuarios" /></form></li>`;
    var contenido = document.getElementById('primera__tabla2');
    contenido.innerHTML = estructura__tabla;
    var contenido2 = document.getElementById('encuestas__cargadas');
    contenido2.innerHTML = encuesta;
}

// para exportar todos los datos a excel

const exportarDatos = () =>{
    let tablaTodosLosDatos = `<tr><td><table id='tabla1'>`;
    tablaTodosLosDatos = tablaTodosLosDatos + `<tr>`;
    database.ref(`/Usuarios/${window.id}/nombre`).on('value', snapshot => {
        let data = snapshot.val();
        tablaTodosLosDatos = tablaTodosLosDatos + `<th>Nombre</th></tr><tr><td>${data}</td></tr></table></td>`;
    });

    database.ref(`/Usuarios/${window.id}/Formularios`).on('value', snapshot => {
        let data = snapshot.val();
        tablaTodosLosDatos = tablaTodosLosDatos + `<td><table id='tabla2'><tr><th>Formularios</th></tr>`;
        for(let array in data){
            tablaTodosLosDatos = tablaTodosLosDatos + `<tr><td>${array}</td></tr>`
        }
        tablaTodosLosDatos = tablaTodosLosDatos + `</table></td>`;
    });

    
    database.ref(`/Usuarios/${window.id}/Respuestas`).on('value', snapshot => {
        let data = snapshot.val();
        tablaTodosLosDatos = tablaTodosLosDatos + `<td><table><tr><th>Respuestas</th></tr>`;
        for(let array in data){
            tablaTodosLosDatos = tablaTodosLosDatos + `<tr><td>${array} id.- ${data[array]}</td></tr>`
        }
        tablaTodosLosDatos = tablaTodosLosDatos + `</table></td>`;
        exportarDatos2(tablaTodosLosDatos); 
    });
}

const exportarDatos2 = (tablaTodosLosDatos) =>{
    tablaTodosLosDatos = tablaTodosLosDatos + `<td><table id='tabla6'><tr>`;
    database.ref(`/Usuarios/${window.id}/Formularios`).on('value', snapshot => {
        let data = snapshot.val();
        tablaTodosLosDatos = tablaTodosLosDatos + `<th colspan="5">Formularios</th></tr>`;
        tablaTodosLosDatos = tablaTodosLosDatos + `<tr><th>id</th><th>Titulo de la encuesta</th><th>descripcion</th>
        <th>preguntas</th><th>opciones</th></tr>`;
        for(let array in data){
            tablaTodosLosDatos = tablaTodosLosDatos + `<tr><td>${array}</td>`;
            database.ref(`/Usuarios/${window.id}/Formularios/${array}/titulo`).on('value', snapshot => {
                let data = snapshot.val();
                tablaTodosLosDatos = tablaTodosLosDatos + `<td>${data}</td>`;
            });
            database.ref(`/Usuarios/${window.id}/Formularios/${array}/descripcion`).on('value', snapshot => {
                let data = snapshot.val();
                tablaTodosLosDatos = tablaTodosLosDatos + `<td>${data}</td>`;
            });
            database.ref(`/Usuarios/${window.id}/Formularios/${array}/preguntas`).on('value', snapshot => {
                let data = snapshot.val();
                let arrayPreguntas = [];
                for(array2 in data){
                    database.ref(`/Usuarios/${window.id}/Formularios/${array}/preguntas/${array2}/pregunta`).on('value', snapshot => {
                        let data = snapshot.val();
                        arrayPreguntas.push(`${array2}.-`);
                        arrayPreguntas.push(`${data} <br>`);

                    });
                }
                tablaTodosLosDatos = tablaTodosLosDatos + `<td>${arrayPreguntas}</td>`;
            });
            database.ref(`/Usuarios/${window.id}/Formularios/${array}/preguntas`).on('value', snapshot => {
                let data = snapshot.val();
                let arrayOpciones = [];
                for(array2 in data){
                    database.ref(`/Usuarios/${window.id}/Formularios/${array}/preguntas/${array2}/opciones`).on('value', snapshot => {
                        let data = snapshot.val();
                        arrayOpciones.push(`${data} <br>`);
                    });
                }
                tablaTodosLosDatos = tablaTodosLosDatos + `<td>${arrayOpciones}</td></tr>`;
            });
        }
        tablaTodosLosDatos = tablaTodosLosDatos + `</table></td></tr>`;
    });

    let contenido2 = document.getElementById('tblData');
    contenido2.innerHTML = tablaTodosLosDatos;
}

const exportarDatos3 = () =>{
}



// cerrar sesión
function cerrar() {
    firebase.auth().signOut()
        .then(function () {
            console.log('Cerrando...');
            //location.reload();
            window.location.href = 'index.html';
        })
        .catch(function (error) {
            alert(error);
        });
}